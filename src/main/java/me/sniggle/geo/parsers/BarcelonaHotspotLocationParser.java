package me.sniggle.geo.parsers;

import me.sniggle.geo.CoordinatesConverter;
import me.sniggle.geo.model.BarcelonaHotspot;
import me.sniggle.geo.model.HotspotType;
import me.sniggle.geo.parsers.xml.GeoCoordinatesConverter;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by iulius on 23/08/15.
 */
public class BarcelonaHotspotLocationParser extends BaseCSVHotspotLocationParser<BarcelonaHotspot> {

  private SAXParser parser;
  private HttpClient client;
  private String requestTemplate;

  public BarcelonaHotspotLocationParser() {
    super();
    SAXParserFactory factory = SAXParserFactory.newInstance();
    factory.setNamespaceAware(true);
    factory.setValidating(true);
    try {
      parser = factory.newSAXParser();
      requestTemplate = IOUtils.toString(Files.newInputStream(Paths.get("src/main/resources/request_template.xml")));
    } catch (ParserConfigurationException | SAXException | IOException e) {
      e.printStackTrace();
    }
    client = HttpClientBuilder.create().build();
  }

  @Override
  protected BarcelonaHotspot postProcessing(BarcelonaHotspot input) {
    if( parser != null ) {
      String requestText = requestTemplate.replaceAll("\\$\\{coordinateX\\}", String.valueOf(input.getCoordinateX()));
      requestText = requestText.replaceAll("\\$\\{coordinateY\\}", String.valueOf(input.getCoordinateY()));
      HttpPost request = new HttpPost("http://geoserveis.icc.cat/arcgis/services/wps/IcgcTransfCoord/GPServer/WPSServer");
      request.setEntity(new ByteArrayEntity(requestText.getBytes()));
      GeoCoordinatesConverter coordinatesConverter = new GeoCoordinatesConverter();
      HttpResponse response = null;
      try {
        response = client.execute(request);
        parser.parse(response.getEntity().getContent(), coordinatesConverter);
        input.setLocation(Double.valueOf(coordinatesConverter.getCoordinateY()), Double.valueOf(coordinatesConverter.getCoordinateX()));
      } catch (IOException | SAXException e) {
        e.printStackTrace();
      }
    }
    return input;
  }

  @Override
  protected BarcelonaHotspot handleLine(String line, int lineNumber) {
    BarcelonaHotspot barcelonaHotspot = new BarcelonaHotspot();
    String[] tokens = line.split(";");
    barcelonaHotspot.setHotspotType(HotspotType.PUBLIC);
    barcelonaHotspot.setProvider("city-of-barcelona");
    barcelonaHotspot.setSsid("WiFI BCN");
    barcelonaHotspot.setCoordinateX(Double.valueOf(tokens[4].replaceAll(",", ".").replaceAll("\\s", "")));
    barcelonaHotspot.setCoordinateY(Double.valueOf(tokens[5].replaceAll(",", ".").replaceAll("\\s", "")));
    return barcelonaHotspot;
  }

}
