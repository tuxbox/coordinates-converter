package me.sniggle.geo.parsers;

import me.sniggle.geo.model.Hotspot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by iulius on 09/08/15.
 */
public abstract class BaseCSVHotspotLocationParser<T extends Hotspot> implements HotspotLocationParser<T> {

  private boolean hasHeader = true;

  public boolean hasHeader() {
    return hasHeader;
  }

  public void setHasHeader(boolean hasHeader) {
    this.hasHeader = hasHeader;
  }

  protected abstract T handleLine(String line, int lineNumber);

  protected boolean verifyLine(String[] csvTokens) {
    return true;
  }

  protected T postProcessing(T input) {
    return input;
  }

  @Override
  public List<T> parse(InputStream in) {
    List<T> result = new ArrayList<>();
    try {
      if (in != null) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        int counterAll = 0, counterContent = 0;
        String line;
        while ((line = reader.readLine()) != null) {
          counterAll++;
          if ((hasHeader() && counterAll > 1) || (!hasHeader())) {
            counterContent++;
            T resultItem = handleLine(line, counterContent);
            if (resultItem != null) {
              result.add(postProcessing(resultItem));
            }
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }
}