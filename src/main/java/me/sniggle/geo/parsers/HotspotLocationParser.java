package me.sniggle.geo.parsers;

import java.io.InputStream;
import java.util.List;

import me.sniggle.geo.model.Hotspot;


/**
 * Created by iulius on 28/07/15.
 */
public interface HotspotLocationParser<T extends Hotspot> {

  public List<T> parse(InputStream in);

}
