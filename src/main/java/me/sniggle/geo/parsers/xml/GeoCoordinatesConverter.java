package me.sniggle.geo.parsers.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by iulius on 23/08/15.
 */
public class GeoCoordinatesConverter extends DefaultHandler {

  private final StringBuffer cache = new StringBuffer();
  private boolean isLiteralData = false;
  private boolean coordinateXresponse = false;
  private String coordinateX;
  private boolean coordinateYresponse = false;
  private String coordinateY;
  private boolean isOutputParameter = false;
  private boolean isIdentifier = false;

  public GeoCoordinatesConverter() {
  }

  public String getCoordinateX() {
    return coordinateX;
  }

  public String getCoordinateY() {
    return coordinateY;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    cache.setLength(0);
    if( "Output".equals(localName) ) {
      isOutputParameter = true;
    } else if( "Identifier".equals(localName) ) {
      isIdentifier = true;
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if( isIdentifier ) {
      cache.append(ch, start, length);
    } else if( coordinateXresponse || coordinateYresponse ) {
      cache.append(ch, start, length);
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    if( "Output".equals(localName) ) {
      isOutputParameter &= false;
    } else if( "Identifier".equals(localName) ) {
      if( cache.toString().equals("Coord_X") ) {
        coordinateXresponse = true;
      } else if( cache.toString().equals("Coord_Y") ) {
        coordinateYresponse = true;
      }
      isIdentifier &= false;
    } else if( "LiteralData".equals(localName) ) {
      if( coordinateXresponse ) {
        coordinateX = cache.toString();
      } else if( coordinateYresponse ) {
        coordinateY = cache.toString();
      }
      coordinateYresponse &= false;
      coordinateXresponse &= false;
    }
  }
}
