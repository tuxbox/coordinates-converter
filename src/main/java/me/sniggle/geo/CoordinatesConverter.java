package me.sniggle.geo;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.sniggle.geo.model.BarcelonaHotspot;
import me.sniggle.geo.parsers.BarcelonaHotspotLocationParser;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by iulius on 23/08/15.
 */
public class CoordinatesConverter {

  public static void main(String[] args) {
    BarcelonaHotspotLocationParser parser = new BarcelonaHotspotLocationParser();
    parser.setHasHeader(true);
    try( InputStream in = Files.newInputStream(Paths.get("src/main/resources/public_hotspots_barcelona.csv")) ){
      List<BarcelonaHotspot> barcelonaHotspotList = parser.parse(in);
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.writeValue(Files.newOutputStream(Paths.get("src/main/resources/public_hotspots_barcelona.json")), barcelonaHotspotList);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
