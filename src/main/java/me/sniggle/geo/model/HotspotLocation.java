package me.sniggle.geo.model;

/**
 * Created by iulius on 28/07/15.
 */
public class HotspotLocation {

  private double longitude;
  private double latitude;

  public HotspotLocation() {
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }
}
