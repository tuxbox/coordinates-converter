package me.sniggle.geo.model;

/**
 * Created by iulius on 23/08/15.
 */
public class Hotspot {

  private String provider = "default";
  private String ssid;
  private HotspotLocation location;
  private HotspotType hotspotType = HotspotType.PUBLIC;

  public Hotspot() {
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public String getSsid() {
    return ssid;
  }

  public void setSsid(String ssid) {
    this.ssid = ssid;
  }

  public HotspotLocation getLocation() {
    return location;
  }

  public void setLocation(HotspotLocation location) {
    this.location = location;
  }

  public void setLocation(double latitude, double longitude) {
    HotspotLocation location = new HotspotLocation();
    location.setLatitude(latitude);
    location.setLongitude(longitude);
    setLocation(location);
  }

  public HotspotType getHotspotType() {
    return hotspotType;
  }

  public void setHotspotType(HotspotType hotspotType) {
    this.hotspotType = hotspotType;
  }
}
