package me.sniggle.geo.model;

/**
 * Created by iulius on 28/07/15.
 */
public enum HotspotType {

  /**
   * free & no registration
   */
  PUBLIC,
  /**
   * free, but with registration
   */
  REGISTRATION_FREE,
  /**
   * paid hotspot, but without registration
   */
  PAID,
  /**
   * paid hotspot & registration required
   */
  REGISTRATION_PAID,
  /**
   * members only
   */
  MEMBERS_ONLY

}
