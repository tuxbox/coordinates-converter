package me.sniggle.geo.model;

/**
 * Created by iulius on 23/08/15.
 */
public class BarcelonaHotspot extends Hotspot {

  private double coordinateX;
  private double coordinateY;

  public BarcelonaHotspot() {
  }

  public double getCoordinateX() {
    return coordinateX;
  }

  public void setCoordinateX(double coordinateX) {
    this.coordinateX = coordinateX;
  }

  public double getCoordinateY() {
    return coordinateY;
  }

  public void setCoordinateY(double coordinateY) {
    this.coordinateY = coordinateY;
  }

}
