import me.sniggle.geo.parsers.xml.GeoCoordinatesConverter;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * Created by iulius on 23/08/15.
 */
public class GeoCoordinatesConverterTest {

  private String expectedCoordinateX = "1.801033713", expectedCoordinateY = "41.545413661";
  private SAXParser parser;

  @Before
  public void setup() throws ParserConfigurationException, SAXException {
    SAXParserFactory factory = SAXParserFactory.newInstance();
    factory.setValidating(true);
    factory.setNamespaceAware(true);
    parser = factory.newSAXParser();
  }

  @Test
  public void parse() throws IOException, SAXException {
    GeoCoordinatesConverter geoCoordinatesConverter = new GeoCoordinatesConverter();
    parser.parse(Files.newInputStream(Paths.get("src/test/resources/test_response.xml")), geoCoordinatesConverter);
    assertEquals(expectedCoordinateX, geoCoordinatesConverter.getCoordinateX());
    assertEquals(expectedCoordinateY, geoCoordinatesConverter.getCoordinateY());
  }

}
